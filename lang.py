import random
# urllib is a library for accessing web data
# note that it works differently between py2 and py3
from urllib.error import HTTPError
from urllib.request import urlopen


class LearnedLanguage:
    def __init__(self):
        """
        first get rid of punctuation and capital letters and split text into words;
        try keeping periods and commas
        """
        self.chars_to_remove = [':', ';', '(', ')', '[', ']', '-', '_', "—", "' ", "',", "'.",
                                " '", '”', '“', '!', '"']
        self.dictionary = {}
        
    def read_text(self, text):
        """
        now read the text and update the pairs as we go
        """
        text = text.replace('--', ' ')
        for char in self.chars_to_remove:
            text = text.replace(char, '')
        text = text.lower().split()  # now a list of lowercase words
        for w_num in range(len(text)-2):
            words_pair = (text[w_num], text[w_num+1])
            self.update_dictionary(words_pair)

    def update_dictionary(self, words_pair):
        """
        here we process a single word pair and add to the dictionary
        """
        first = words_pair[0]  # initial word
        second = words_pair[1]  # following word
        # if first occurence of word, we create a dict of followers with one entry:
        if first not in self.dictionary.keys():
            self.dictionary[first] = {second:1}
        # if first already in dict, then only update frequencies of followers
        else:
            if second in self.dictionary[first].keys():
                self.dictionary[first][second] += 1
            else:
                self.dictionary[first][second] = 1

    def generate_text(self, length=500, init_word=False):
        """
        now the other part: drawing and printing Markov chains of words
        we try to capitalize after periods
        """
        if not init_word:
            lkey = list(self.dictionary.keys())
            weights = [sum([self.dictionary[k][x] for x in self.dictionary[k].keys()])for k in lkey]
            sum_weights = sum(weights)
            init_word = random.choices(lkey, weights=[w/sum_weights for w in weights])[0]
        caps = True
        for i in range(length):
            if caps:
                print(init_word[0].upper() + init_word[1:], end=' ')
            else:
                print(init_word, end=' ')
            if init_word.endswith('.'):
                caps = True
            else:
                caps = False
            init_word = self.draw_next_word(init_word)
        print('\n')

    def draw_next_word(self, init_word):
        """
        here is a probabilistic model for drawing next word based on a previous one
        """
        dict_of_followers = self.dictionary[init_word]
        possible = list(dict_of_followers.keys())
        scores = [dict_of_followers[x] for x in possible]
        sum_scores = sum(scores)
        probabilities = [float(x)/sum_scores for x in scores]  # just normalize sum to 1
        #chosen_word = np.random.choice(possible, p=probabilities)
        chosen_word = random.choices(possible, weights=probabilities)[0]
        return chosen_word
    
    def load_books(self, books=None, n=5):
        """
        now we take several books from Project Gutenberg,
        download in text format and analyze them altogether;
        e.g. Tom Sawyer is number 74, Ulysses is 4300; you can
        look it up at http://www.gutenberg.org/ebooks/search/?sort_order=downloads
        """
        if books is None:
            books = [random.randint(1, 25000) for _ in range(n)]
        for book_number in books:
            # 'urlopen' returns an object very similar to the output of 'open'
            try:
                ebook = urlopen('http://www.gutenberg.org/files/{n}/{n}-0.txt'.format(n=book_number))
            except HTTPError:
                try:
                    ebook = urlopen('http://www.gutenberg.org/cache/epub/{n}/pg{n}.txt'.format(n=book_number))
                except HTTPError:
                    # if the book is nowhere to be found, try another one
                    books.append(random.randint(1, 25000))
                    continue
            # but in py3 there is an additional issue with encoding (hence the decode() fn)
            book_raw = ebook.read().decode('utf-8')
            # now we try to find the title
            lead_line1 = book_raw[:book_raw.find('This eBook')].strip().split()
            lead_line2 = book_raw[:book_raw.find("Copyright laws")].strip().split()
            lead_line = lead_line1 if len(lead_line1) < len(lead_line2) else lead_line2
            print("book nr {}: ".format(book_number), end='')
            try:
                print(' '.join(lead_line[lead_line.index('of') + 1:]))
            except ValueError:
                try:
                    print(' '.join(lead_line[lead_line.index("Gutenberg's") + 1:]))
                except ValueError:
                    print(lead_line)
            # there are marks for where the book starts and ends, so we trim the text:
            book_start = book_raw.find('*** START OF THIS PROJECT')
            book_end = book_raw.find('*** END OF THIS PROJECT')
            book_trimmed = book_raw[book_start:book_end]
            self.read_text(book_trimmed)
        print("\n")
        
        
lang = LearnedLanguage()

# we might choose an own set of books by uncommenting:
# book_numbers = [4300, 350, 122, 74, 1080]
# otherwise we go with a random set:
book_numbers = None
lang.load_books(book_numbers)
# by default, we generate 500 words
lang.generate_text()

